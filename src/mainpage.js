import {fromEvent, Subject, interval, zip, from, forkJoin, timer } from 'rxjs';
import { Adventurer } from './adventurer';
import { addAdventurer, getAdventurers, checkAdventurerStatus, getAdventurer, updateAdventurer, updateAdventurerDisplay, deleteAdventurer } from './adventurerService';
import { debounceTime, map, filter, scan, take, takeUntil, repeat, pairwise, sampleTime, switchMap } from 'rxjs/operators';
import { displayQuestion, displayAnswer } from './questionsService'
import { addAdventurerStats, displayAdventurerStats, gatherAdventurerStats, prepareAndUpdateAdventurerStats, resetQuestionsCounters, deleteAdventurerStats } from './adventurerStatsService';
import { AdventurerStats } from './adventurerStats';



 
export class Mainpage{
    constructor(){
        const stopRequesting = new Subject();
        const adventurersurl = `http://localhost:3000/adventurers/`;
        const statsurl = `http://localhost:3000/stats/`;
        let questionsCounter = 0;
        let adminMode = true;


         let welcomeContainer = document.createElement("div");
         welcomeContainer.className = "welcomeContainer";
         document.body.appendChild(welcomeContainer);
    
         let welcomeLabel = document.createElement("h1");
         welcomeLabel.innerHTML = "WELCOME BOOKDRAGON";
         welcomeLabel.className = "wpage";
         welcomeContainer.appendChild(welcomeLabel);
    
         let startAdventureLabel = document.createElement("label");
         startAdventureLabel.innerHTML = "-START YOUR ADVENTURE NOW-";
         startAdventureLabel.className = "startAdventureLabel";
         welcomeContainer.appendChild(startAdventureLabel);
        
         let containerforplayerInfo = document.createElement("div");
         containerforplayerInfo.className = "containerforplayerInfo";
         welcomeContainer.appendChild(containerforplayerInfo);
    
         let adventurerNameLabel = document.createElement("label");
         adventurerNameLabel.innerHTML = "Enter your Adventurer Name";
         adventurerNameLabel.className = "adventurerNameLabel";
         containerforplayerInfo.appendChild(adventurerNameLabel);

    
         let adventurerNameInput = document.createElement("input");
         adventurerNameInput.className = "adventurerNameInput";
         containerforplayerInfo.appendChild(adventurerNameInput);


         let adventurerNameCheckImg = document.createElement("IMG");
         adventurerNameCheckImg.className = "adventurerNameCheck";
         adventurerNameCheckImg.src = "/images/arrow_left.png";
         containerforplayerInfo.appendChild(adventurerNameCheckImg);
         

         let adventurerLoaded = document.createElement("div");
         adventurerLoaded.className = "adventurerLoaded";

         let startQuizbutton = document.createElement("button");
         startQuizbutton.innerHTML = "Start Game";
         startQuizbutton.className = "startQuiz";
         containerforplayerInfo.appendChild(startQuizbutton);

         let registerForm = document.createElement("form");
         registerForm.id = "postData";
         containerforplayerInfo.appendChild(registerForm);

         let registerAdventurerButton = document.createElement("input");
         registerAdventurerButton.className = "registerAdventurer";
         registerAdventurerButton.type = "submit";
         registerAdventurerButton.value = "Register";
         registerForm.appendChild(registerAdventurerButton);

       
        let adventurerInfoContainer = document.createElement("div");
        adventurerInfoContainer.className = "adventurerInfo";
        adventurerLoaded.appendChild(adventurerInfoContainer);

        let currentAdventurerNameLabel = document.createElement("label");
        currentAdventurerNameLabel.className = "currentAdventurerLabel";
        adventurerInfoContainer.appendChild(currentAdventurerNameLabel);

        let currentAdventurerRankLabel = document.createElement("label");
        currentAdventurerRankLabel.className = "currentAdventurerRankLabel";
        adventurerInfoContainer.appendChild(currentAdventurerRankLabel);

        let currentAdventurerRankIcon = document.createElement("IMG");
        currentAdventurerRankIcon.className = "currentAdventurerRankIcon";
        adventurerInfoContainer.appendChild(currentAdventurerRankIcon);

        let currentAdventurerHighscoreLabel = document.createElement("label");
        currentAdventurerHighscoreLabel.className = "currentAdventurerHighscoreLabel";
        adventurerInfoContainer.appendChild(currentAdventurerHighscoreLabel);

        let quizOptionsContainer = document.createElement("div");
        quizOptionsContainer.className = "quizOptionsContainer";
        adventurerLoaded.appendChild(quizOptionsContainer);

        let quizOptionsLabel = document.createElement("label");
        quizOptionsLabel.className = "quizOptionsLabel";
        quizOptionsLabel.innerHTML = "Are you ready to test your BOOKDRAGON skills?";
        quizOptionsContainer.appendChild(quizOptionsLabel);

        let gameStartButton = document.createElement("button");
        gameStartButton.innerHTML = "Get Started";
        gameStartButton.className = "startGameButtonClassic";
        quizOptionsContainer.appendChild(gameStartButton);

        let viewRangListButton = document.createElement("button");
        viewRangListButton.innerHTML = "Rang List";
        viewRangListButton.className = "viewRangListButton";
        quizOptionsContainer.appendChild(viewRangListButton);


        let recommendImage = document.createElement("img");
        recommendImage.src = "/images/namespell.jpg";
        recommendImage.className = "recommendImage";
        quizOptionsContainer.appendChild(recommendImage);

        let bookdragonImg = document.createElement("img");
        bookdragonImg.className = "recommendImage";
        quizOptionsContainer.appendChild(bookdragonImg);


        let recommendLabel = document.createElement("label");
        recommendLabel.innerHTML = "";
        recommendLabel.className = "recommendLabel";
        quizOptionsContainer.appendChild(recommendLabel);
       
        let bestAdventurerLabel = document.createElement("label");
        bestAdventurerLabel.innerHTML = "";
        bestAdventurerLabel.className = "bestAdventurerLabel";
        quizOptionsContainer.appendChild(bestAdventurerLabel);

        let adventurerStatsContainer = document.createElement("div");
        adventurerStatsContainer.className = "adventurerStatsContainer";
        
        let adventurerStatsLabel = document.createElement("label");
        adventurerStatsLabel.innerHTML = "Adventurer Stats";
        adventurerStatsLabel.className = "adventurerStatsLabel";
        adventurerStatsContainer.appendChild(adventurerStatsLabel);

        let authorQuestionsStatsLabel = document.createElement("label");
        authorQuestionsStatsLabel.className = "questionsStatsLabel";
        authorQuestionsStatsLabel.innerHTML = "Author questions: ";
        adventurerStatsContainer.appendChild(authorQuestionsStatsLabel);

        let bookTitleQuestionsStatsLabel = document.createElement("label");
        bookTitleQuestionsStatsLabel.className = "questionsStatsLabel";
        bookTitleQuestionsStatsLabel.innerHTML = "Book Title questions: ";
        adventurerStatsContainer.appendChild(bookTitleQuestionsStatsLabel);

        let releaseDateQuestionsStatsLabel = document.createElement("label");
        releaseDateQuestionsStatsLabel.className = "questionsStatsLabel";
        releaseDateQuestionsStatsLabel.innerHTML = "Release Date questions: ";
        adventurerStatsContainer.appendChild(releaseDateQuestionsStatsLabel);

        let genreQuestionsStatsLabel = document.createElement("label");
        genreQuestionsStatsLabel.className = "questionsStatsLabel";
        genreQuestionsStatsLabel.innerHTML = "Genre questions: ";
        adventurerStatsContainer.appendChild(genreQuestionsStatsLabel);

        let checkStatsButton = document.createElement("button");
        checkStatsButton.innerHTML = "Adventurer Stats";
        checkStatsButton.className = "statsButton";
        quizOptionsContainer.appendChild(checkStatsButton);

        let closeStatsButton = document.createElement("button");
        closeStatsButton.innerHTML = "Adventurer Stats";
        closeStatsButton.className = "statsButton";

        let adminFieldContainer = document.createElement("div");
        adminFieldContainer.className = "adminFieldContainer";

        let adminFieldLabel = document.createElement("label");
        adminFieldLabel.className = "adminFieldLabel";
        adminFieldLabel.innerHTML = "Admin: ON";
        adminFieldContainer.appendChild(adminFieldLabel);

        let adminOptionLabel = document.createElement("label"); 
        adminOptionLabel.className = "adminOptionLabel";
        adminOptionLabel.innerHTML = "Type adventurer name:"
        adminFieldContainer.appendChild(adminOptionLabel);

        let adminInputSearch = document.createElement("input");
        adminInputSearch.placeholder = "Adventurer Name";
        adminInputSearch.className = "adminInputSearch";
        adminFieldContainer.appendChild(adminInputSearch);

        let adventurerFoundLabel = document.createElement("label");
        adventurerFoundLabel.className = "adventurerFoundLabel";

        let backToSearchButton = document.createElement("button");
        backToSearchButton.className = "backToSearchButton";
        backToSearchButton.innerHTML = "X";
        backToSearchButton.style.visibility = "hidden";
        adminFieldContainer.appendChild(backToSearchButton);


        let deleteAdventurerButton = document.createElement("button");
        deleteAdventurerButton.innerHTML = "Delete Adventurer";
        deleteAdventurerButton.className = "deleteAdventurerButton";
        deleteAdventurerButton.disabled = true;
        adminFieldContainer.appendChild(deleteAdventurerButton);

        let gameContainer = document.createElement("div");
        gameContainer.className = "classicGameContainer";

        let rangListContainer = document.createElement("div");
        rangListContainer.className = "rangListContainer";

        let gameIntroLabel = document.createElement("label");
        gameIntroLabel.innerHTML = "Answer 10 questions and earn your BOOKDRAGON title.";
        gameIntroLabel.className = "classicGameIntroLabel"
        gameContainer.appendChild(gameIntroLabel);
        

        let gameQuestionsContainer = document.createElement("div");
        gameQuestionsContainer.className = "classicGameQuestionsContainer";
        gameContainer.appendChild(gameQuestionsContainer);

       
        let questionsLabel = document.createElement("label");
        questionsLabel.className = "questionsLabelClassic";
        questionsLabel.innerHTML = "QUESTIONS";
        gameQuestionsContainer.appendChild(questionsLabel);

        let helpButton = document.createElement("button");
        helpButton.className = "helpButton";
        helpButton.innerHTML = "?";
        gameQuestionsContainer.appendChild(helpButton);
        

        let gameAnswersContainer = document.createElement("div");
        gameAnswersContainer.className = "classicGameAnswersContainer";
        gameContainer.appendChild(gameAnswersContainer);
       
        let gameHelpContainer = document.createElement("div");
        gameHelpContainer.className = "gameHelpContainer";


        let helpLabel = document.createElement("label");
        helpLabel.className = "helpLabel";
        helpLabel.innerHTML = "Help"
        gameHelpContainer.appendChild(helpLabel);

        let helpText = document.createElement("label");
        helpText.className = "helpText";
        helpText.innerHTML = "Earn points by answering questions. If you answer correctly, you will earn 20 points. "
                            + "If you give wrong answer you will lose 20 points (you can go below zero). "
                            + "If you are not sure about your answer, you can skip question by clicking next, "
                            + "and stay on the same digits(keep your current points). If you are not satisfied "
                            + "by your progress, you can abort quiz at any moment and start again (other reasons included). "
                            + "When you answer 10 questions game ends and you can click finish button. By clicking finish button "
                            + "if you have better score in this try than your highscore, your new highscore will be updated, "
                            + "same as your title. "
        gameHelpContainer.appendChild(helpText);

        let closeHelpButton = document.createElement("button");
        closeHelpButton.className = "closeHelpButton";
        closeHelpButton.innerHTML = "Close";
        gameHelpContainer.appendChild(closeHelpButton);

        let gameAnswerAButton = document.createElement("Button");
        gameAnswerAButton.className = "AnswerButton btn-block";
        gameAnswerAButton.innerHTML = "AnswerA";
        gameAnswersContainer.appendChild(gameAnswerAButton);

        let gameAnswerBButton = document.createElement("Button");
        gameAnswerBButton.className = "AnswerButton btn-block";
        gameAnswerBButton.innerHTML = "AnswerB";
        gameAnswersContainer.appendChild(gameAnswerBButton);

        let gameAnswerCButton = document.createElement("Button");
        gameAnswerCButton.className = "AnswerButton btn-block";
        gameAnswerCButton.innerHTML = "AnswerC";
        gameAnswersContainer.appendChild(gameAnswerCButton);

        let gameAnswerDButton = document.createElement("Button");
        gameAnswerDButton.className = "AnswerButton btn-block";
        gameAnswerDButton.innerHTML = "AnswerD";
        gameAnswersContainer.appendChild(gameAnswerDButton);

        let correctAnswerButton = document.createElement("Button");
        correctAnswerButton.className = "correctAnswerButton";
        gameAnswersContainer.appendChild(correctAnswerButton); 

        let yourAnswerLabel = document.createElement("label");
        yourAnswerLabel.className = "yourAnswerLabel";
        yourAnswerLabel.innerHTML = "Your answer: ";
        gameContainer.appendChild(yourAnswerLabel);

        let correctAnswerLabel = document.createElement("label");
        correctAnswerLabel.className = "correctAnswerLabel";
        correctAnswerLabel.innerHTML = "Correct answer: ";
        gameContainer.appendChild(correctAnswerLabel);

        let scorePointsLabel = document.createElement("label");
        scorePointsLabel.className = "scorePointsLabel";
        scorePointsLabel.innerHTML = "Current score: 0" + " Questions answered:  0"
        gameContainer.appendChild(scorePointsLabel);


        let nextQuestionButton = document.createElement("Button");
        nextQuestionButton.innerHTML = "Next";
        nextQuestionButton.className = "nextQuestionButton";
        gameContainer.appendChild(nextQuestionButton);

        let finishButton = document.createElement("Button");
        finishButton.innerHTML = "Finish";
        finishButton.className = "finishButton";
        finishButton.disabled = true;
        gameContainer.appendChild(finishButton);

        let abortQuiz = document.createElement("button");
        abortQuiz.innerHTML = "Abort Game";
        abortQuiz.className = "abortQuizClassic";
        gameContainer.appendChild(abortQuiz);

        let rangListLabel = document.createElement("label");
        rangListLabel.innerHTML = "-LEADERBOARD-";
        rangListLabel.className = "rangListLabel";
        rangListContainer.appendChild(rangListLabel);

        rangListTableCreate() 
        
        let refreshTableButton = document.createElement("button");
        refreshTableButton.innerHTML = "Refresh Table";
        refreshTableButton.className = "refreshTable";
        rangListContainer.appendChild(refreshTableButton);


        let backToAdventurerPageButton = document.createElement("button");
        backToAdventurerPageButton.innerHTML = "Back to Adventurer Page";
        backToAdventurerPageButton.className = "backToAdventurerPageButton";
        rangListContainer.appendChild(backToAdventurerPageButton);

        let backToLoginPage = document.createElement("button");
        backToLoginPage.className = "backToLoginPage";
        backToLoginPage.innerHTML = "<- Back";
        adventurerLoaded.appendChild(backToLoginPage);

        fromEvent(adventurerNameInput, 'input')
        .pipe(
            debounceTime(500),
            map(event => event.target.value),
            filter(text => text.length >= 1)
           ).subscribe((value) => {
               getAdventurers()
               .then(response=>response.json())
               .then(data => {
                       let registered = false;
                       (data).forEach(element => {
                       if(element.id === value.toLowerCase()){
                        registered = true;
                       }
            
                    })
                   if(registered === true){
                            forbidRegistration()
                   }
                   else{
                            forbidStart()
                   }
                })    
           })

         


        fromEvent(registerAdventurerButton, "click").subscribe(() => { 
            if(adventurerNameInput.value !== "" && adventurerNameInput.value !== undefined){  
            let adventurer = 
            new Adventurer(adventurerNameInput.value.toLocaleLowerCase(), adventurerNameInput.value, "Dragon-Egg", 0, "regular")
            addAdventurer(adventurersurl, adventurer)
            alert("You have registered successfully!") 
            let stats = new AdventurerStats(adventurerNameInput.value.toLocaleLowerCase(), 0, 0, 0, 0, 0, 0, 0, 0)
            addAdventurerStats(statsurl, stats)              
            }
            else{
                alert("You have to specify your adventurer name first!")
            }
        }
        );
         

        fromEvent(currentAdventurerNameLabel, "click").subscribe(() => {   
            getAdventurer(currentAdventurerNameLabel.value.toLocaleLowerCase())
            .then(response => response.json())
            .then(data => {
                if(data["privileges"] === "admin"){
                    if(adminMode){
                    document.getElementsByClassName("quizOptionsContainer")[0]
                    .replaceChild(adminFieldContainer, bookdragonImg)
                    adminMode = false;
                    checkStatsButton.style.visibility = "hidden";
                    }
                    else{
                        document.getElementsByClassName("quizOptionsContainer")[0]
                       .replaceChild(bookdragonImg, adminFieldContainer)
                       adminMode = true;
                       checkStatsButton.style.visibility = "visible";
                       }
                }
            })
            }
        );    

        fromEvent(adminInputSearch, "input")
        .pipe(
            debounceTime(900),
            map(event => event.target.value.toLocaleLowerCase()),
            filter(text => text.length >= 1),
            switchMap(id=> getAdventurer(id))
        ).subscribe(value => {
            if(value.ok){
            value.json().then(data => {
            adventurerFoundLabel.innerHTML = "Adventurer found: " + data["adventurerName"];
            adventurerFoundLabel.value = data["adventurerName"];
            document.getElementsByClassName("adminFieldContainer")[0]
            .replaceChild(adventurerFoundLabel, adminInputSearch);
            backToSearchButton.style.visibility = "visible";
            deleteAdventurerButton.disabled = false;
            })}}
        );

        fromEvent(backToSearchButton, "click").subscribe(() =>{
            document.getElementsByClassName("adminFieldContainer")[0]
            .replaceChild(adminInputSearch, adventurerFoundLabel)
            backToSearchButton.style.visibility = "hidden";
            deleteAdventurerButton.disabled = true;
            adminInputSearch.value = "";
        });


        fromEvent(deleteAdventurerButton, "click").subscribe(() =>{
            deleteAdventurerButton.disabled = true;
            let remove = confirm("Still wanna delete this advanturer?");
            if(remove === true){
            deleteAdventurer(adventurersurl + "/" + adventurerFoundLabel.value.toLocaleLowerCase());
            alert("Adventurer deleted!");
            deleteAdventurerStats(statsurl + "/" + adventurerFoundLabel.value.toLocaleLowerCase());
            }
            adminInputSearch.value = "";
            backToSearchButton.style.visibility = "hidden";
            document.getElementsByClassName("adminFieldContainer")[0]
            .replaceChild(adminInputSearch, adventurerFoundLabel)
        })
         
        fromEvent(startQuizbutton, "click").subscribe(() => {   
            if(adventurerNameInput.value !== "" && adventurerNameInput.value !== undefined){
            checkAdventurerStatus(adventurerNameInput.value.toLowerCase(), adventurerLoaded, welcomeContainer)
            }
            else{
                alert("You have to specify your adventurer name first!")
            }
        }
        );
    


        fromEvent(recommendImage, 'mouseover').subscribe(() => {
            recommend(document.getElementsByClassName("recommendLabel")[0])
            bestAdventurer().subscribe( best => 
            { document.getElementsByClassName("bestAdventurerLabel")[0].innerHTML = 
                                              "Best adventurer " + "-" +
                                              best[1]["adventurerName"] + "-"});
            recommendImage.style.display = "none";
            checkStatsButton.style.visibility = "visible";
            bookdragonImg.src = "/images/bookdragon.jpg";
        })

        fromEvent(checkStatsButton, "click").subscribe(() => {
            document.getElementsByClassName("quizOptionsContainer")[0]
            .replaceChild(adventurerStatsContainer, bookdragonImg)
            closeStatsButton.style.visibility = "visible";
            document.getElementsByClassName("quizOptionsContainer")[0]
            .replaceChild(closeStatsButton, checkStatsButton)
           
            displayAdventurerStats(document.getElementsByClassName("currentAdventurerLabel")[0].value, 
                                   document.getElementsByClassName("questionsStatsLabel")[0], 
                                   document.getElementsByClassName("questionsStatsLabel")[1], 
                                   document.getElementsByClassName("questionsStatsLabel")[2], 
                                   document.getElementsByClassName("questionsStatsLabel")[3])
        })


        fromEvent(closeStatsButton, "click").subscribe(() => {
            document.getElementsByClassName("quizOptionsContainer")[0]
            .replaceChild(bookdragonImg, adventurerStatsContainer)

            document.getElementsByClassName("quizOptionsContainer")[0]
            .replaceChild(checkStatsButton, closeStatsButton)
            
        })

        fromEvent(gameStartButton, "click").subscribe(() => {
            document.getElementsByClassName("adventurerLoaded")[0]
            .replaceChild(gameContainer, quizOptionsContainer);
            let randBtn = parseInt(Math.random()*4);
            resetQuestionsCounters();
            displayQuestion(document.getElementsByClassName("questionsLabelClassic")[0],
                            document.getElementsByClassName("AnswerButton")[randBtn], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+1)%4], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+2)%4], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+3)%4],
                            document.getElementsByClassName("correctAnswerButton")[0])}
        )

        fromEvent(viewRangListButton, "click").subscribe(() => {
            document.getElementsByClassName("adventurerLoaded")[0]
            .replaceChild(rangListContainer, quizOptionsContainer)
        }
        )


        fromEvent(finishButton, "click").subscribe(()=>{
            getAdventurer(document.getElementsByClassName("currentAdventurerLabel")[0].value).then(
                response=>response.json())
            .then(data => {
                const currentScore = document.getElementsByClassName("scorePointsLabel")[0].value;
                if(data["highscore"] > currentScore)
                {
                    alert("Your highscore is: " + data["highscore"])
                    finishButton.disabled = true;
                    questionsCounter = 0;
                    enableAnswerButtons()
                    nextQuestionButton.disabled = false;
                    resetScoreLabels();
                    nextQuestionButton.className = "nextQuestionButton"
                    document.getElementsByClassName("adventurerLoaded")[0]
                    .replaceChild(quizOptionsContainer, gameContainer); 
                    prepareAndUpdateAdventurerStats(document.getElementsByClassName("currentAdventurerLabel")[0].value.toLocaleLowerCase());
                }
                else{
                    let rank;
                    if(currentScore > 160){
                        rank = "Dragon-King"
                    }
                    if(currentScore <= 160){
                        rank = "Dragon-Commander"
                    }
                    if(currentScore <= 120){
                        rank = "Dragon-Warrior"
                    }
                    if(currentScore <= 80){
                        rank = "Dragon-Apprentice"
                    }
                    if(currentScore <= 40){
                        rank = "Dragon-Whelp"
                    }
                    if(currentScore <= 0){
                        rank = "Dragon-Egg"
                    }
                    prepareAndUpdateAdventurerStats(document.getElementsByClassName("currentAdventurerLabel")[0].value.toLocaleLowerCase());
                    let adventurer = new Adventurer(data["id"], data["adventurerName"], rank, currentScore, data["privileges"])
                    updateAdventurer(`${adventurersurl}${data["id"]}`, adventurer)
                    alert("Title and highscore updated.");
                    finishButton.disabled = true;
                    questionsCounter = 0;
                    enableAnswerButtons()
                    nextQuestionButton.disabled = false;
                    resetScoreLabels();
                    nextQuestionButton.className = "nextQuestionButton"
                    document.getElementsByClassName("adventurerLoaded")[0]
                    .replaceChild(quizOptionsContainer, gameContainer);  
                    updateAdventurerDisplay(adventurer, document.getElementsByClassName("currentAdventurerRankLabel")[0],
                                                        document.getElementsByClassName("currentAdventurerRankIcon")[0], 
                                                        document.getElementsByClassName("currentAdventurerHighscoreLabel")[0] );    
                }
            })

        })


        fromEvent(nextQuestionButton, "click")
        .pipe(
          map(val => 20),
          takeUntil(stopRequesting),
          take(10),
          scan((totalScore, current) =>{
              if(nextQuestionButton.className === "nextQuestionButtonCorrect"){
                totalScore += current;
                return totalScore;
              }
              else if(nextQuestionButton.className === "nextQuestionButtonWrong"){
                  totalScore -= current;
                  return totalScore;
              }
              else{
                  return totalScore;
              }
          }, 0),
          repeat()
        )
        .subscribe((totalScore) => {
            gatherAdventurerStats(document.getElementsByClassName("questionsLabelClassic")[0],
                                  document.getElementsByClassName("yourAnswerLabel")[0],
                                  document.getElementsByClassName("correctAnswerLabel")[0] );
            questionsCounter++;
            if(questionsCounter > 10){
                questionsCounter =1
            }
            let randBtn = parseInt(Math.random()*4);
            displayQuestion(document.getElementsByClassName("questionsLabelClassic")[0], 
                            document.getElementsByClassName("AnswerButton")[randBtn], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+1)%4], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+2)%4], 
                            document.getElementsByClassName("AnswerButton")[(randBtn+3)%4],
                            document.getElementsByClassName("correctAnswerButton")[0])
            nextQuestionButton.className = "nextQuestionButton"
            enableAnswerButtons()
            document.getElementsByClassName("yourAnswerLabel")[0].innerHTML = "Your answer: ";
            document.getElementsByClassName("correctAnswerLabel")[0].innerHTML = "Correct answer: ";
            document.getElementsByClassName("scorePointsLabel")[0].innerHTML = "Current score: " + totalScore + 
            " Questions answered: "  +  questionsCounter;
            document.getElementsByClassName("scorePointsLabel")[0].value = totalScore;
            if(questionsCounter === 10){
                nextQuestionButton.disabled = true;
                finishButton.disabled = false;
                disableAnswerButtons()
            }
            
        })

        fromEvent(helpButton, "click").subscribe(() =>{
            document.getElementsByClassName("classicGameContainer")[0].replaceChild(gameHelpContainer, gameAnswersContainer);
            helpButton.disabled = true;
        })

        fromEvent(closeHelpButton, "click").subscribe(() => {
            document.getElementsByClassName("classicGameContainer")[0].replaceChild(gameAnswersContainer, gameHelpContainer);
            helpButton.disabled = false;
        })
           
       fromEvent(gameAnswerAButton, "click").pipe(
        debounceTime(800)
     )
     .subscribe(() => 
     {   
         displayAnswer(document.getElementsByClassName("yourAnswerLabel")[0] ,
                       document.getElementsByClassName("AnswerButton")[0], 
                       document.getElementsByClassName("correctAnswerLabel")[0], 
                       document.getElementsByClassName("correctAnswerButton")[0])   
         disableAnswerButtons()
 })

     fromEvent(gameAnswerBButton, "click").pipe(
         debounceTime(800)
      )
      .subscribe(() => 
      { 
         displayAnswer(document.getElementsByClassName("yourAnswerLabel")[0] ,
                       document.getElementsByClassName("AnswerButton")[1], 
                       document.getElementsByClassName("correctAnswerLabel")[0], 
                       document.getElementsByClassName("correctAnswerButton")[0])
         disableAnswerButtons()     
  })


        fromEvent(gameAnswerCButton, "click").pipe(
            debounceTime(800)
         )
         .subscribe(() => 
         {
            displayAnswer(document.getElementsByClassName("yourAnswerLabel")[0] ,
                          document.getElementsByClassName("AnswerButton")[2], 
                          document.getElementsByClassName("correctAnswerLabel")[0], 
                          document.getElementsByClassName("correctAnswerButton")[0])
            disableAnswerButtons()   
     })
 

        fromEvent(gameAnswerDButton, "click").pipe(
            debounceTime(800)
         )
         .subscribe(() => 
         {  
            displayAnswer(document.getElementsByClassName("yourAnswerLabel")[0] ,
                          document.getElementsByClassName("AnswerButton")[3], 
                          document.getElementsByClassName("correctAnswerLabel")[0], 
                          document.getElementsByClassName("correctAnswerButton")[0])
            disableAnswerButtons()     
     })

        fromEvent(abortQuiz, "click").subscribe(() => {
            stopRequesting.next(true);
            finishButton.disabled = true;
            questionsCounter = 0;
            enableAnswerButtons()
            nextQuestionButton.disabled = false;
            resetScoreLabels();
            nextQuestionButton.className = "nextQuestionButton"
            document.getElementsByClassName("adventurerLoaded")[0]
            .replaceChild(quizOptionsContainer, gameContainer)    
        })
         

        fromEvent(refreshTableButton, "click")
        .pipe(
            sampleTime(2000)
        )
        .subscribe(() =>{ 
            refreshRangTable()          
         })

        fromEvent(backToAdventurerPageButton, "click").subscribe(() =>{ 
            document.getElementsByClassName("adventurerLoaded")[0]
            .replaceChild(quizOptionsContainer, rangListContainer)
        })
        

        fromEvent(backToLoginPage, "click").subscribe(() => document.body.replaceChild(welcomeContainer, adventurerLoaded));

        function resetScoreLabels(){
            document.getElementsByClassName("yourAnswerLabel")[0].innerHTML = "Your answer: ";
            document.getElementsByClassName("correctAnswerLabel")[0].innerHTML = "Correct answer: ";
            document.getElementsByClassName("scorePointsLabel")[0].innerHTML = "Current score: 0" + "    Questions answered: 0";
        }


        function enableAnswerButtons(){
            gameAnswerAButton.disabled = false;
            gameAnswerBButton.disabled = false;
            gameAnswerCButton.disabled = false;
            gameAnswerDButton.disabled = false;
        }

        function disableAnswerButtons(){
            gameAnswerAButton.disabled = true;
            gameAnswerBButton.disabled = true;
            gameAnswerCButton.disabled = true;
            gameAnswerDButton.disabled = true;  
        }

        function forbidRegistration(){
            adventurerNameCheckImg.src = "/images/approved.jpg"
            registerAdventurerButton.disabled = true;
            startQuizbutton.disabled = false;
            startQuizbutton.className = "startQuizbuttonAvailable";
            registerAdventurerButton.className = "registerAdventurer"
       }

       function forbidStart(){
            adventurerNameCheckImg.src = "/images/SignAlert.png"
            registerAdventurerButton.disabled = false;
            startQuizbutton.disabled = true;
            startQuizbutton.className = "startQuiz";
            registerAdventurerButton.className = "registerAdventurerAvailable";
       }



        function rangListTableCreate(){
  
            let rangListTable = document.createElement("table");
            rangListTable.className = "rangListTable";
            rangListContainer.appendChild(rangListTable);
    
            let rangListTHead = document.createElement("thead");
            rangListTable.appendChild(rangListTHead);
    
            let rangListTBody = document.createElement("tbody");
            rangListTable.appendChild(rangListTBody);
    
            let rangListHeadRow = document.createElement("tr");
            ["#rank" , "Adventurer", "Title", "Highscore"].forEach(el=>{
                let tHead = document.createElement("th");
                tHead.className = "tHead";
                tHead.appendChild(document.createTextNode(el));
                rangListHeadRow.appendChild(tHead);
            })
            rangListTHead.appendChild(rangListHeadRow)
            getAdventurers().then(response => response.json()).then(
                data=>
                {   
                    data.sort((a,b) =>{return (a.highscore < b.highscore) ? 1 : ((b.highscore < a.highscore) ? -1 : 0);} )
                    for(let i = 1; i < 11; i++){
                        let tableRow = document.createElement("tr");
                        tableRow.className = "tableRow";
                        for( let d = 0; d < 4; d++){
                            let tData = document.createElement("td");
                            tData.className = "tData";
                            if( d === 0){
                                tData.textContent = i;
                            }
                            else if( d === 1 ){
                                tData.textContent = data[i-1]["adventurerName"];
                            }
                            else if( d === 2 ){
                                tData.textContent = data[i-1]["rank"];
                            }
                            else {
                                tData.textContent = data[i-1]["highscore"];
                            }
                            tableRow.appendChild(tData);
                        }

                        rangListTBody.appendChild(tableRow)
                    }
                    
            })  
    
            }

        function refreshRangTable(){
            getAdventurers().then(response => response.json()).then(data=>{
                data.sort((a,b) =>{return (a.highscore < b.highscore) ? 1 : ((b.highscore < a.highscore) ? -1 : 0);} )
                let rows = document.getElementsByClassName("tableRow");
                for(let i=0; i<rows.length; i++){
                 rows[i].children[1].innerHTML = data[i]["adventurerName"]
                 rows[i].children[2].innerHTML = data[i]["rank"];
                 rows[i].children[3].innerHTML = data[i]["highscore"];
                }
              }
            ) 
         }

         function recommend(recommendLab){
            const $timer = interval(3000);
            const $books = from(["Dragon Hunter", "It", "Inferno", "The Hobbit", "Harry Potter and the Philosopher's Stone", 
                                 "The Kite Runner", "The Idiot", "The Name of the Rose", "The bridge on the Drina", 
                                 "Murder on the Orient Express", "Digital Fortress", "To Kill a Mockingbird", "The Picture of Dorian Gray",
                                 "END"]);
            zip($books, $timer).pipe(
                pairwise(),
                filter(pair =>{
                    const currentState = pair[0];
                    return  currentState[0] !== "END";
                }),
                map(pair => pair[0][0]),
                repeat()
            ).subscribe(book =>{
                recommendLab.innerHTML = "Recommended BOOK  \"" + book + "\" " 
                }
            )
            }

        function bestAdventurer(){
            const $time = timer(1000)
            const $bestAdventurer = from(getAdventurers().then(response => response.json()).then(data => {
                let best = data[0]
                for(let i=1; i<data.length;i++){
                    if(best["highscore"] < data[i]["highscore"]){
                        best = data[i]
                    }
                }
                return best
            }))
           return forkJoin($time, $bestAdventurer) 
        }
        
        
    }


}