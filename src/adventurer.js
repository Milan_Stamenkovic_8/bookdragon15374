export class Adventurer{
    constructor(id, adventurerName, rank, highscore, privileges){
        this.id = id;
        this.adventurerName = adventurerName;
        this.rank = rank;
        this.highscore = highscore;
        this.privileges = privileges;
    }
}