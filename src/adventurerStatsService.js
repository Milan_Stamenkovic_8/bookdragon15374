import { from } from 'rxjs';
import { AdventurerStats } from './adventurerStats';

const statsurl = `http://localhost:3000/stats/`;

export let authorQuestions = 0;
export let authorQuestionsCorrect = 0;

export let bookTitleQuestions = 0;
export let bookTitleQuestionsCorrect = 0;

export let releaseDateQuestions = 0;
export let releaseDateQuestionsCorrect = 0;

export let genreQuestions = 0;
export let genreQuestionsCorrect = 0;

export function getAdventurerStats(id){
    return fetch(`${statsurl}${id}`)
}

export function addAdventurerStats(url = ``, data = {}) {
  
    return fetch(url, {
        method: "POST",
        mode: "cors", 
        cache: "no-cache", 
        credentials: "same-origin", 
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow", 
        referrer: "no-referrer",
        body: JSON.stringify(data), 
    })
    .then(response => response.json()); 
}

export function updateAdventurerStats(url = ``, data = {}){
    return fetch(url, {
        method: "PUT",
        mode: "cors", 
        cache: "no-cache", 
        credentials: "same-origin", 
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow", 
        referrer: "no-referrer",
        body: JSON.stringify(data), 
    })
    .then(response => response.json()); 
}

export function deleteAdventurerStats(url = ``){
    return fetch(url, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        }
    })
}

export function displayAdventurerStats(adventurer, authorLabel, titleLabel, releaseDateLabel, genreLabel){
    const stats = from(getAdventurerStats(adventurer).then(response => response.json()))
    stats.subscribe(
        data => {
            if( data["authorQuestions"] !== 0){
            let authorQuestionsPercentage =  data["authorQuestionsCorrect"] / data["authorQuestions"] * 100;
            authorLabel.innerHTML = "Author questions: " + data["authorQuestionsCorrect"] + "/" + data["authorQuestions"] +
            " = " + authorQuestionsPercentage.toFixed(2) + "%";
            }
            else{
            authorLabel.innerHTML = "Author questions: " + 0 + "/" + 0 + " = " + 0 + "%";    
            }

            if(data["bookTitleQuestions"] !== 0){
            let titleQuestionsPercentage = data["bookTitleQuestionsCorrect"] / data["bookTitleQuestions"] * 100;
            titleLabel.innerHTML = "Book Title questions: " + data["bookTitleQuestionsCorrect"] + "/" + data["bookTitleQuestions"] +
            " = " + titleQuestionsPercentage.toFixed(2) + "%";
            }
            else{
            titleLabel.innerHTML = "Book Title questions: " + 0 + "/" + 0 + " = " + 0 + "%";
            }

            if(data["releaseDateQuestions"] !== 0){
            let releaseDateQuestionsPercentage = data["releaseDateQuestionsCorrect"] / data["releaseDateQuestions"] * 100;
            releaseDateLabel.innerHTML = "Release Date questions: " + data["releaseDateQuestionsCorrect"] + "/" + 
            data["releaseDateQuestions"]  + " = " + releaseDateQuestionsPercentage.toFixed(2) + "%";
            }
            else{
            releaseDateLabel.innerHTML = "Release Date questions: " + 0 + "/" + 0 + " = " + 0 + "%";
            }

            if(data["genreQuestions"] !== 0){
            let genreQuestionsPercentage = data["genreQuestionsCorrect"] / data["genreQuestions"] * 100;
            genreLabel.innerHTML = "Genre questions: " + data["genreQuestionsCorrect"] + "/" + data["genreQuestions"] +
            " = " + genreQuestionsPercentage.toFixed(2) + "%";
            }
            else{
            genreLabel.innerHTML = "Genre questions: " + 0 + "/" + 0 + " = " + 0 + "%";
            }
        }
    )
}

export function gatherAdventurerStats(questionsLabel, yourAnswerLabel, correctAnswerLabel){
    if(questionsLabel.value === "author" ){
        authorQuestions += 1;
        if(yourAnswerLabel.value === correctAnswerLabel.value && yourAnswerLabel.value !== undefined)
        authorQuestionsCorrect += 1;
    }
    else if(questionsLabel.value === "title"){
        bookTitleQuestions += 1;
        if(yourAnswerLabel.value === correctAnswerLabel.value && yourAnswerLabel.value !== undefined)
        bookTitleQuestionsCorrect += 1;
    }
    else if(questionsLabel.value === "releaseDate"){
        releaseDateQuestions += 1;
        if(yourAnswerLabel.value === correctAnswerLabel.value && yourAnswerLabel.value !== undefined)
        releaseDateQuestionsCorrect += 1;
    }
    else{
        genreQuestions += 1;
        if(yourAnswerLabel.value === correctAnswerLabel.value && yourAnswerLabel.value !== undefined)
        genreQuestionsCorrect += 1;
    }
}


export async function authorQuestionsCount(){
    return new Promise((resolve, reject) => {
        setTimeout(() => 
            authorQuestions >= 0 ?
            resolve(authorQuestions) :
            reject("No author questions")
            , 500);
    });
}

export async function bookTitleQuestionsCount(){
    return new Promise((resolve, reject) => {
        setTimeout(() => 
            bookTitleQuestions >= 0 ?
            resolve(bookTitleQuestions) :
            reject("No book title questions")
            , 500);
    });
}

export async function releaseDateQuestionsCount(){
    return new Promise((resolve, reject) => {
        setTimeout(() => 
            releaseDateQuestions >= 0 ?
            resolve(releaseDateQuestions) :
            reject("No release date questions")
            , 500);
    });
}

export async function genreQuestionsCount(){
    return new Promise((resolve, reject) => {
        setTimeout(() => 
            genreQuestions >= 0 ?
            resolve(genreQuestions) :
            reject("No book title questions")
            , 500);
    });
}

export function prepareAndUpdateAdventurerStats(adventurerId){
    
    Promise.all([authorQuestionsCount(), bookTitleQuestionsCount(), releaseDateQuestionsCount(), genreQuestionsCount()]).then(values => {
    const $oldStats = from(getAdventurerStats(adventurerId).then(response => response.json()).then(data => data))
    $oldStats.subscribe(stats => {
        authorQuestions += stats["authorQuestions"];
        authorQuestionsCorrect += stats["authorQuestionsCorrect"];
        bookTitleQuestions += stats["bookTitleQuestions"];
        bookTitleQuestionsCorrect += stats["bookTitleQuestionsCorrect"];
        releaseDateQuestions += stats["releaseDateQuestions"];
        releaseDateQuestionsCorrect += stats["releaseDateQuestionsCorrect"];
        genreQuestions += stats["genreQuestions"];
        genreQuestionsCorrect += stats["genreQuestionsCorrect"];
        const newStats = new AdventurerStats(adventurerId, authorQuestions, authorQuestionsCorrect, 
                                             bookTitleQuestions, bookTitleQuestionsCorrect,
                                             releaseDateQuestions, releaseDateQuestionsCorrect, 
                                             genreQuestions, genreQuestionsCorrect);
        updateAdventurerStats(`${statsurl}${adventurerId}`, newStats);
        })
    })
}

export function resetQuestionsCounters(){
    authorQuestions = 0;
    authorQuestionsCorrect = 0;

    bookTitleQuestions = 0;
    bookTitleQuestionsCorrect = 0;

    releaseDateQuestions = 0;
    releaseDateQuestionsCorrect = 0;

    genreQuestions = 0;
    genreQuestionsCorrect = 0;
}