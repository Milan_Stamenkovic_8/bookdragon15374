import { getBooks } from './bookService'
import {  from, zip } from 'rxjs';

const questionsurl = `http://localhost:3000/questions/`;


export function getQuestions(){
    return fetch(questionsurl)
}

export function displayQuestion(questionsLabel, answerButton1, answerButton2, answerButton3, answerButton4, correctAnswer){
  
    const $questions = from(getQuestions().then(response => response.json()))
    const $books = from(getBooks().then(response => response.json()))
    zip($questions, $books).subscribe(response=>{
        let randID = parseInt(Math.random() *4);
        let randIsbn = parseInt(Math.random() * 20);
        if(response[0][randID]["about"] === "author"){
            questionsLabel.innerHTML = "Who wrote the " +  "\"" + response[1][randIsbn]["title"] + "\""+ " released "  + 
                                        response[1][randIsbn]["releaseDate"]+ "?"
            questionsLabel.value = "author"
            answerButton1.innerHTML = response[1][randIsbn]["author"]
            answerButton2.innerHTML = response[1][(randIsbn+5)%20]["author"]
            answerButton3.innerHTML = response[1][(randIsbn+10)%20]["author"]
            answerButton4.innerHTML = response[1][(randIsbn+15)%20]["author"]
            correctAnswer.value = response[1][randIsbn]["author"]
        }
        else if(response[0][randID]["about"] === "title"){
            questionsLabel.innerHTML = "What is the title of a book written by " +  response[1][randIsbn]["author"] + " released " + 
                                        response[1][randIsbn]["releaseDate"] + "?"
            questionsLabel.value = "title"
            answerButton1.innerHTML = response[1][randIsbn]["title"]
            answerButton2.innerHTML = response[1][(randIsbn+7)%20]["title"]
            answerButton3.innerHTML = response[1][(randIsbn+12)%20]["title"]
            answerButton4.innerHTML = response[1][(randIsbn+17)%20]["title"]
            correctAnswer.value = response[1][randIsbn]["title"]
        }
        else if(response[0][randID]["about"] === "releaseDate"){
            questionsLabel.innerHTML = "When was the book " + "\""+response[1][randIsbn]["title"] + "\""+" by " + 
                                        response[1][randIsbn]["author"] +  " released?"
            questionsLabel.value = "releaseDate"
            answerButton1.innerHTML = response[1][randIsbn]["releaseDate"]
            answerButton2.innerHTML = response[1][(randIsbn+8)%20]["releaseDate"]
            answerButton3.innerHTML = response[1][(randIsbn+13)%20]["releaseDate"]
            answerButton4.innerHTML = response[1][(randIsbn+18)%20]["releaseDate"]
            correctAnswer.value = response[1][randIsbn]["releaseDate"]
        }
        else{
            questionsLabel.innerHTML = "What genre is the book " + "\""+ response[1][randIsbn]["title"] +"\""+ " by " + 
                                        response[1][randIsbn]["author"] + "?"
            questionsLabel.value = "genre"
            answerButton1.innerHTML = response[1][randIsbn]["genre"]
            answerButton2.innerHTML = response[1][(randIsbn+2)%20]["genre"]
            answerButton3.innerHTML = response[1][(randIsbn+7)%20]["genre"]
            answerButton4.innerHTML = response[1][(randIsbn+12)%20]["genre"]
            correctAnswer.value = response[1][randIsbn]["genre"]
        }

    })
}  


export function displayAnswer(answerLabel, answerButton, correctLabel, correctAnswer){
    answerLabel.innerHTML = "Your answer: " + answerButton.innerHTML;
    answerLabel.value = answerButton.innerHTML;
    correctLabel.innerHTML = "Correct answer: " + correctAnswer.value;
    correctLabel.value = correctAnswer.value;
    if(answerButton.innerHTML === correctAnswer.value){
        document.getElementsByClassName("nextQuestionButton")[0].className ="nextQuestionButtonCorrect";
    }
    else{
        document.getElementsByClassName("nextQuestionButton")[0].className ="nextQuestionButtonWrong";
    }
}    
