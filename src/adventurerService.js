import { from } from 'rxjs';
import { Adventurer } from './adventurer'

const adventurersurl = `http://localhost:3000/adventurers/`;

export function addAdventurer(url = ``, data = {}) {
  
      return fetch(url, {
          method: "POST",
          mode: "cors", 
          cache: "no-cache", 
          credentials: "same-origin", 
          headers: {
              "Content-Type": "application/json",
          },
          redirect: "follow", 
          referrer: "no-referrer",
          body: JSON.stringify(data), 
      })
      .then(response => response.json()); 
  }

  export function getAdventurers(){
       return fetch(adventurersurl)
       
  }

  export function getAdventurer(id){
    return fetch(`${adventurersurl}${id}`)
    
}


  export function checkAdventurerStatus(id, adventurerLoaded, welcomeContainer ) {
    let  adventurer;
    from(
        fetch(`${adventurersurl}${id}`)
        .then(response => response.json())
        .then((data) => {
            alert("Welcome: " + data["adventurerName"])
            document.body.replaceChild(adventurerLoaded, welcomeContainer)
            return adventurer = new Adventurer(data["id"], data["adventurerName"], data["rank"], data["highscore"], data["privileges"])
        
    }).then((adventurer) => {
        currentAdventurer(adventurer, document.getElementsByClassName("currentAdventurerLabel")[0], 
                                      document.getElementsByClassName("currentAdventurerRankLabel")[0],
                                      document.getElementsByClassName("currentAdventurerRankIcon")[0], 
                                      document.getElementsByClassName("currentAdventurerHighscoreLabel")[0])     
    })
    )
}
export function currentAdventurer(adventurer, currentAdventurerNameLabel, currentAdventurerRankLabel, 
                                  currentAdventurerRankIcon, currentAdventurerHighscoreLabel)
{
    currentAdventurerNameLabel.innerHTML = "Adventurer name: " + adventurer.adventurerName
    currentAdventurerNameLabel.value = adventurer.id
    currentAdventurerRankLabel.innerHTML = "Rank: " + adventurer.rank;
    if(adventurer.rank === "Dragon-Egg"){
        currentAdventurerRankIcon.src = "/images/dragon_egg.png";
    }
    else if(adventurer.rank === "Dragon-Whelp"){
        currentAdventurerRankIcon.src = "/images/dragon1.png";
    }
    else if(adventurer.rank === "Dragon-Apprentice"){
        currentAdventurerRankIcon.src = "/images/dragon2.png";
    }
    else if(adventurer.rank === "Dragon-Warrior"){
        currentAdventurerRankIcon.src = "/images/dragon3.png";
    }
    else if(adventurer.rank === "Dragon-Commander"){
        currentAdventurerRankIcon.src = "/images/dragon4.png";
    }
    else{
        currentAdventurerRankIcon.src = "/images/dragon5.png";
    }
    currentAdventurerHighscoreLabel.innerHTML = "Highscore: " + adventurer.highscore + " points"; 
}

export function updateAdventurer(url = ``, data = {}){
    return fetch(url, {
        method: "PUT",
        mode: "cors", 
        cache: "no-cache", 
        credentials: "same-origin", 
        headers: {
            "Content-Type": "application/json",
        },
        redirect: "follow", 
        referrer: "no-referrer",
        body: JSON.stringify(data), 
    })
    .then(response => response.json()); 
}

export function deleteAdventurer(url = ``){
    return fetch(url, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        }
    })
}

export function updateAdventurerDisplay(adventurer, currentAdventurerRankLabel, currentAdventurerRankIcon, currentAdventurerHighscoreLabel){
    currentAdventurerRankLabel.innerHTML = "Rank: " + adventurer.rank;
    if(adventurer.rank === "Dragon-Egg"){
        currentAdventurerRankIcon.src = "/images/dragon_egg.png";
    }
    else if(adventurer.rank === "Dragon-Whelp"){
        currentAdventurerRankIcon.src = "/images/dragon1.png";
    }
    else if(adventurer.rank === "Dragon-Apprentice"){
        currentAdventurerRankIcon.src = "/images/dragon2.png";
    }
    else if(adventurer.rank === "Dragon-Warrior"){
        currentAdventurerRankIcon.src = "/images/dragon3.png";
    }
    else if(adventurer.rank === "Dragon-Commander"){
        currentAdventurerRankIcon.src = "/images/dragon4.png";
    }
    else{
        currentAdventurerRankIcon.src = "/images/dragon5.png";
    }
    currentAdventurerHighscoreLabel.innerHTML = "Highscore: " + adventurer.highscore + " points"; 
}
