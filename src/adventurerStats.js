export class AdventurerStats {
    constructor(id, authorQuestions, authorQuestionsCorrect, bookTitleQuestions, bookTitleQuestionsCorrect, 
                releaseDateQuestions, releaseDateQuestionsCorrect, genreQuestions, genreQuestionsCorrect){
                    
                    this.id = id;
                    this.authorQuestions = authorQuestions;
                    this.authorQuestionsCorrect = authorQuestionsCorrect;
                    this.bookTitleQuestions = bookTitleQuestions;
                    this.bookTitleQuestionsCorrect = bookTitleQuestionsCorrect;
                    this.releaseDateQuestions = releaseDateQuestions;
                    this.releaseDateQuestionsCorrect = releaseDateQuestionsCorrect;
                    this.genreQuestions = genreQuestions;
                    this.genreQuestionsCorrect = genreQuestionsCorrect;
                }

}